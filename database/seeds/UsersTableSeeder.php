<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'company_id' => 1,
            // 'customer_id' => '',
            'role_id' => 1,
            'name' => 'test',
            'phone' => 'xxxx',
            'email' => 'demosuperadmin@wiraenergi.com',
            'password' => app('hash')->make('demosuperadmin'),
            'address' => 'xxx',
            'email_verified_at' => Carbon::now(),
            'image_profile' => 'xx',
            'status' => 1,
            'remember_token' => str_random(100)
        ]);
    }
}
