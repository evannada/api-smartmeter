<?php

use Illuminate\Database\Seeder;

class ServicetypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicetypes')->insert([
            'id' => 1,
            'name' => 'Gas',
            'unit' => 'M3'
        ]);

        DB::table('servicetypes')->insert([
            'id' => 2,
            'name' => 'Electricity',
            'unit' => 'KWH'
        ]);

        DB::table('servicetypes')->insert([
            'id' => 3,
            'name' => 'Water',
            'unit' => 'M3'
        ]);
    }
}
