<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'id' => 1,
            'networkserver_id' => 1,
            'company_name' => 'Wira Energi',
            'company_phone' => 'xxxxx',
            'company_email' => 'info@wiraenergi.co.id',
            'company_address' => 'SOHO PODOMORO CITY (NEO SOHO) LANTAI 40 UNIT 09',
            'company_website' => 'http://wiraenergi.co.id/',
            'image_company' => '',
            'pic_name' => 'wiraenergi',
            'pic_phone' => 'xxxxx',
            'pic_email' => 'info@wiraenergi.co.id',
            'method' => 'api',
            'url_middleware' => 'https://google.com/', 
            'mqtt_username' => '', 
            'mqtt_password' => '',
            'mqtt_port' => '', 
            'status' => 1,
        ]);
    }
}
