<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'superadmin'
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'admin'
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'finance'
        ]);

        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'technician'
        ]);

        DB::table('roles')->insert([
            'id' => 5,
            'name' => 'customer'
        ]);
    }
}
