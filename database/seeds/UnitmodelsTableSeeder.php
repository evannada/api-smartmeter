<?php

use Illuminate\Database\Seeder;

class UnitmodelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unitmodels')->insert([
            'id' => 1,
            'name' => 'Sindcon',
        ]);

        DB::table('unitmodels')->insert([
            'id' => 2,
            'name' => 'Kerlink',
        ]);
    }
}
