<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            NetworkserversTableSeeder::class,
            CompaniesTableSeeder::class,
            CustomersTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            PowersourcesTableSeeder::class,
            ServicetypesTableSeeder::class,
            UnitmodelsTableSeeder::class,
            DevicetypesTableSeeder::class,
            GatewaysTableSeeder::class,
            NodesTableSeeder::class,
        ]);
    }
}
