<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->unsignedBigInteger('gateway_id');
            $table->foreign('gateway_id')->references('id')->on('gateways')->onDelete('cascade');
            $table->unsignedBigInteger('devicetype_id');
            $table->foreign('devicetype_id')->references('id')->on('devicetypes')->onDelete('cascade');
            $table->unsignedBigInteger('servicetype_id');
            $table->foreign('servicetype_id')->references('id')->on('servicetypes')->onDelete('cascade');
            $table->dateTime('date_installation');
            $table->string('device_eui')->unique();
            $table->string('mac_address');
            $table->string('meter_number')->nullable();
            $table->string('cronjob')->nullable(); 
            $table->string('image_node')->nullable();
            $table->string('battery')->nullable();
            $table->string('valve')->nullable();
            $table->string('rssi')->nullable();
            $table->string('noise')->nullable();
            $table->string('last_usage')->nullable();
            $table->string('last_balance')->nullable();
            $table->dateTime('last_update')->nullable();  
            $table->tinyInteger('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
