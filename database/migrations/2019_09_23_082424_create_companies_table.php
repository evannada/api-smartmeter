<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('networkserver_id');
            $table->foreign('networkserver_id')->references('id')->on('networkservers')->onDelete('cascade');
            $table->string('company_name');
            $table->string('company_phone');
            $table->string('company_email');
            $table->string('company_address');
            $table->string('company_website')->nullable();
            $table->string('image_company')->nullable();         
            $table->string('pic_name');
            $table->string('pic_phone');
            $table->string('pic_email');
            $table->string('method');
            $table->string('url_middleware');
            $table->string('mqtt_username')->nullable();
            $table->string('mqtt_password')->nullable();
            $table->string('mqtt_port')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
