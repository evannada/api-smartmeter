<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gateway;
use Illuminate\Support\Facades\Validator;

class GatewayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Gateway::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $gateways = $query->get();
        $gateways->load(['unitmodel', 'powersource']);

        $response = [
            'status' => 'success',
            'data' => $gateways
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'unitmodel_id' => 'required|exists:unitmodels,id',
            'powersource_id' => 'required|exists:powersources,id',
            'date_installation' => 'required|date_format:Y-m-d H:i:s',
            'gateway_name' => 'required|string',
            'mac' => 'required|string|unique:gateways,mac',
            'description' => 'present|nullable|string',
            'sim1_provider' => 'required|string',
            'sim1_number' => 'required|string',
            'sim2_provider' => 'present|nullable|string',
            'sim2_name' => 'present|nullable|string',
            'image_gateway' => 'present|nullable|string',
            'last_update' => 'present|nullable|date_format:Y-m-d H:i:s',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_gateway)) {
            $image_gateway = substr($request->image_gateway, strpos($request->image_gateway, ",")+1);
            $image_gateway_name = time() . '.' . str_random(20) . '.jpg'; 
            $image_gateway_path = public_path() . "/gateway/" . $image_gateway_name;
            file_put_contents($image_gateway_path, base64_decode($image_gateway));
            $image_gateway_url = env('API_URL'). "/gateway/" . $image_gateway_name;
        } else {
            $image_gateway_url = env('API_URL'). "/other/no_image.png";
        }

        $gateway = Gateway::create([
            'company_id' => $request->company_id,
            'unitmodel_id' => $request->unitmodel_id,
            'powersource_id' => $request->powersource_id,
            'date_installation' => $request->date_installation,
            'gateway_name' => $request->gateway_name,
            'mac' => $request->mac,
            'description' => $request->description,
            'sim1_provider' => $request->sim1_provider,
            'sim1_number' => $request->sim1_number,
            'sim2_provider' => $request->sim2_provider,
            'sim2_name' => $request->sim2_name,
            'image_gateway' => $image_gateway_url,
            'last_update' => $request->last_update,
            'status' => $request->status,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $gateway
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Gateway::query();
        $query = $query->whereCompany_id($request->company_id);

        $gateway = $query->findOrFail($id);
        $gateway->load(['unitmodel', 'powersource', 'nodes']);

        $response = [
            'status' => 'success',
            'data' => $gateway
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'unitmodel_id' => 'required|exists:unitmodels,id',
            'powersource_id' => 'required|exists:powersources,id',
            'date_installation' => 'required|date_format:Y-m-d H:i:s',
            'gateway_name' => 'required|string',
            // 'mac' => 'required|string|unique:gateways,mac',
            'description' => 'present|nullable|string',
            'sim1_provider' => 'required|string',
            'sim1_number' => 'required|string',
            'sim2_provider' => 'present|nullable|string',
            'sim2_name' => 'present|nullable|string',
            'image_gateway' => 'present|nullable|string',
            'last_update' => 'present|nullable|date_format:Y-m-d H:i:s',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $gateway = Gateway::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_gateway)) {
            $image_gateway = substr($request->image_gateway, strpos($request->image_gateway, ",")+1);
            $image_gateway_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_gateway_path = public_path() . "/gateway/" . $image_gateway_name;
            file_put_contents($image_gateway_path, base64_decode($image_gateway));
            $image_gateway_url = env('API_URL'). "/gateway/" . $image_gateway_name;
            $gateway->update([
                'image_gateway' => $image_gateway_url,
            ]);
        }   
        
        $gateway->update([
            'company_id' => $request->company_id,
            'unitmodel_id' => $request->unitmodel_id,
            'powersource_id' => $request->powersource_id,
            'date_installation' => $request->date_installation,
            'gateway_name' => $request->gateway_name,
            // 'mac' => $request->mac,
            'description' => $request->description,
            'sim1_provider' => $request->sim1_provider,
            'sim1_number' => $request->sim1_number,
            'sim2_provider' => $request->sim2_provider,
            'sim2_name' => $request->sim2_name,
            // 'image_gateway' => $image_gateway_url,
            'last_update' => $request->last_update,
            'status' => $request->status,
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $gateway
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $gateway = Gateway::whereCompany_id($request->company_id)->findOrFail($id);
        $gateway->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}