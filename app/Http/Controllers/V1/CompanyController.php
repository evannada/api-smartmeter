<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Company::query();

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $companies = $query->get();
        $companies->load('networkserver');

        $response = [
            'status' => 'success',
            'data' => $companies
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'networkserver_id' => 'required|exists:networkservers,id',
            'company_name' => 'required|string',
            'company_phone' => 'required|string',
            'company_email' => 'required|email',
            'company_address' => 'required|string',
            'company_website' => 'present|nullable|string',
            'image_company' => 'present|nullable|string',
            'pic_name' => 'required|string',
            'pic_phone' => 'required|string',
            'pic_email' => 'required|email',
            'method' => 'required|string',
            'url_middleware' => 'required|string', 
            'mqtt_username' => 'present|nullable|string',
            'mqtt_password' => 'present|nullable|string',
            'mqtt_port' => 'present|nullable|string',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_company)) {
            $image_company = substr($request->image_company, strpos($request->image_company, ",")+1);
            $image_company_name = time() . '.' . str_random(20) . '.jpg'; 
            $image_company_path = public_path() . "/company/" . $image_company_name;
            file_put_contents($image_company_path, base64_decode($image_company));
            $image_company_url = env('API_URL'). "/company/" . $image_company_name;
        } else {
            $image_company_url = env('API_URL'). "/other/no_image.png";
        }

        $company = Company::create([
            'networkserver_id' => $request->networkserver_id,
            'company_name' => $request->company_name,
            'company_phone' => $request->company_phone,
            'company_email' => $request->company_email,
            'company_address' => $request->company_address,
            'company_website' => $request->company_website,
            'image_company' => $image_company_url,
            'pic_name' => $request->pic_name,
            'pic_phone' => $request->pic_phone,
            'pic_email' => $request->pic_email,
            'method' => $request->method,
            'url_middleware' => $request->url_middleware, 
            'mqtt_username' => $request->mqtt_username,
            'mqtt_password' => $request->mqtt_password,
            'mqtt_port' => $request->mqtt_port,
            'status' => $request->status,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $company
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::with('networkserver')->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $company
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'networkserver_id' => 'required|exists:networkservers,id',
            'company_name' => 'required|string',
            'company_phone' => 'required|string',
            'company_email' => 'required|email',
            'company_address' => 'required|string',
            'company_website' => 'present|nullable|string',
            'image_company' => 'present|nullable|string',
            'pic_name' => 'required|string',
            'pic_phone' => 'required|string',
            'pic_email' => 'required|email',
            'method' => 'required|string',
            'url_middleware' => 'required|string', 
            'mqtt_username' => 'present|nullable|string',
            'mqtt_password' => 'present|nullable|string',
            'mqtt_port' => 'present|nullable|string',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Company::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_company)) {
            $image_company = substr($request->image_company, strpos($request->image_company, ",")+1);
            $image_company_name = time() . '.' . str_random(10) . '.jpg'; 
            $image_company_path = public_path() . "/company/" . $image_company_name;
            file_put_contents($image_company_path, base64_decode($image_company));
            $image_company_url = env('API_URL'). "/company/" . $image_company_name;
            $company->update([
                'image_company' => $image_company_url,
            ]);
        }   
        
        $company->update([
            'networkserver_id' => $request->networkserver_id,
            'company_name' => $request->company_name,
            'company_phone' => $request->company_phone,
            'company_email' => $request->company_email,
            'company_address' => $request->company_address,
            'company_website' => $request->company_website,
            // 'image_company' => $image_company_url,
            'pic_name' => $request->pic_name,
            'pic_phone' => $request->pic_phone,
            'pic_email' => $request->pic_email,
            'method' => $request->method,
            'url_middleware' => $request->url_middleware, 
            'mqtt_username' => $request->mqtt_username,
            'mqtt_password' => $request->mqtt_password,
            'mqtt_port' => $request->mqtt_port,
            'status' => $request->status,
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $company
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        $company->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}