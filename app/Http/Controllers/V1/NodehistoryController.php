<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Nodehistory;
use Illuminate\Support\Facades\Validator;

class NodehistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'node_id' => 'nullable|exists:nodes,id',
            'device_eui' => 'nullable|string',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Nodehistory::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('node_id')) {
            $query = $query->whereNode_id($request->node_id);
        }

        if ($request->has('device_eui')) {
            $query = $query->whereDevice_eui($request->device_eui);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $nodehistories = $query->get();

        $response = [
            'status' => 'success',
            'data' => $nodehistories
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Nodehistory::query();
        $query = $query->whereCompany_id($request->company_id);

        $nodehistory = $query->findOrFail($id);
        $nodehistory->load('node');

        $response = [
            'status' => 'success',
            'data' => $nodehistory
        ];
        return response()->json($response, 200);
    }


}