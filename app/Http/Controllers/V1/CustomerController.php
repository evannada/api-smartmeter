<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = Customer::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $customers = $query->get();

        $response = [
            'status' => 'success',
            'data' => $customers
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_name' => 'required|string',
            'customer_phone' => 'required|string',
            'customer_email' => 'required|email',
            'customer_address' => 'required|string',
            'customer_website' => 'present|nullable|string',
            'image_customer' => 'present|nullable|string',
            'pic_name' => 'required|string',
            'pic_phone' => 'required|string',
            'pic_email' => 'required|email',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_customer)) {
            $image_customer = substr($request->image_customer, strpos($request->image_customer, ",")+1);
            $customer_name = time() . '.' . str_random(20) . '.jpg'; 
            $customer_path = public_path() . "/customer/" . $customer_name;
            file_put_contents($customer_path, base64_decode($image_customer));
            $customer_url = env('API_URL'). "/customer/" . $customer_name;
        } else {
            $customer_url = env('API_URL'). "/other/no_image.png";
        }

        $company = Customer::create([
            'company_id' => $request->company_id,
            'customer_name' => $request->customer_name,
            'customer_phone' => $request->customer_phone,
            'customer_email' => $request->customer_email,
            'customer_address' => $request->customer_address,
            'customer_website' => $request->customer_website,
            'image_customer' => $customer_url,
            'pic_name' => $request->pic_name,
            'pic_phone' => $request->pic_phone,
            'pic_email' => $request->pic_email,
            'status' => $request->status,
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $company
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Customer::whereCompany_id($request->company_id)->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $company
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_name' => 'required|string',
            'customer_phone' => 'required|string',
            'customer_email' => 'required|email',
            'customer_address' => 'required|string',
            'customer_website' => 'present|nullable|string',
            'image_customer' => 'present|nullable|string',
            'pic_name' => 'required|string',
            'pic_phone' => 'required|string',
            'pic_email' => 'required|email',
            'status' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Customer::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_customer)) {
            $image_customer = substr($request->image_customer, strpos($request->image_customer, ",")+1);
            $customer_name = time() . '.' . str_random(10) . '.jpg'; 
            $customer_path = public_path() . "/customer/" . $customer_name;
            file_put_contents($customer_path, base64_decode($image_customer));
            $customer_url = env('API_URL'). "/customer/" . $customer_name;
            $company->update([
                'image_customer' => $customer_url,
            ]);
        }   
        
        $company->update([
            'company_id' => $request->company_id,
            'customer_name' => $request->customer_name,
            'customer_phone' => $request->customer_phone,
            'customer_email' => $request->customer_email,
            'customer_address' => $request->customer_address,
            'customer_website' => $request->customer_website,
            // 'image_customer' => $customer_url,
            'pic_name' => $request->pic_name,
            'pic_phone' => $request->pic_phone,
            'pic_email' => $request->pic_email,
            'status' => $request->status,
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $company
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $company = Customer::whereCompany_id($request->company_id)->findOrFail($id);
        $company->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}