<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_id' => 'nullable|exists:customers,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = User::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('customer_id')) {
            $query = $query->whereCustomer_id($request->customer_id);
        }

        if ($request->has('offset')) {
            $query = $query->offset($request->offset);
        }

        if ($request->has('limit')) {
            $query = $query->limit($request->limit);
        }
        
        $users = $query->get();

        $response = [
            'status' => 'success',
            'data' => $users
        ];
        return response()->json($response, 200);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_id' => 'present|nullable|exists:customers,id',
            'role_id' => 'required|exists:roles,id',
            'name' => 'required|string',
            'phone' => 'present|nullable',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8',
            'address' => 'present|nullable|string',
            // 'email_verified_at' => 'nullable',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer',
            // 'remember_token' => 'nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $profile_name = time() . '.' . str_random(20) . '.jpg'; 
            $profile_path = public_path() . "/user/" . $profile_name;
            file_put_contents($profile_path, base64_decode($image_profile));
            $profile_url = env('API_URL'). "/user/" . $profile_name;
        } else {
            $profile_url = env('API_URL'). "/other/no_image.png";
        }

        $user = User::create([
            'company_id' => $request->company_id,
            'customer_id' => $request->customer_id,
            'role_id' => $request->role_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => app('hash')->make($request->password),
            'address' => $request->address,
            // 'email_verified_at' => 'nullable',
            'image_profile' => $profile_url,
            'status' => $request->status,
            'remember_token' => str_random(100)
        ]);

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $user
        ];
        return response()->json($response, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_id' => 'nullable|exists:customers,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = User::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('customer_id')) {
            $query = $query->whereCustomer_id($request->customer_id);
        }

        $user = $query->findOrFail($id);

        $response = [
            'status' => 'success',
            'data' => $user
        ];
        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_id' => 'present|nullable|exists:customers,id',
            'role_id' => 'required|exists:roles,id',
            'name' => 'required|string',
            'phone' => 'present|nullable',
            // 'email' => 'required|email|unique:users,email',
            // 'password' => 'required|string|min:8',
            'address' => 'present|nullable|string',
            // 'email_verified_at' => 'nullable',
            'image_profile' => 'present|nullable|string',
            'status' => 'required|integer',
            // 'remember_token' => 'nullable'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::findOrFail($id);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->image_profile)) {
            $image_profile = substr($request->image_profile, strpos($request->image_profile, ",")+1);
            $profile_name = time() . '.' . str_random(10) . '.jpg'; 
            $profile_path = public_path() . "/user/" . $profile_name;
            file_put_contents($profile_path, base64_decode($image_profile));
            $profile_url = env('API_URL'). "/user/" . $profile_name;
            $user->update([
                'image_profle' => $profile_url,
            ]);
        }   
        
        $user->update([
            'company_id' => $request->company_id,
            'customer_id' => $request->customer_id,
            'role_id' => $request->role_id,
            'name' => $request->name,
            'phone' => $request->phone,
            // 'email' => $request->email,
            // 'password' => app('hash')->make($request->password),
            'address' => $request->address,
            // 'email_verified_at' => 'nullable',
            // 'image_profile' => $profile_url,
            'status' => $request->status,
            // 'remember_token' => str_random(100)
        ]);
        
        $response = [
            'status' => 'success',
            'message' => 'Record updated successfully.',
            'data' =>  $user
        ];
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:companies,id',
            'customer_id' => 'present|nullable|exists:customers,id',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $query = User::query();
        $query = $query->whereCompany_id($request->company_id);

        if ($request->has('customer_id')) {
            $query = $query->whereCustomer_id($request->customer_id);
        }
        
        $user = $query->findOrFail($id);
        $user->delete();

        $response = [
            'status' => 'success',
            'message' => 'Record deleted successfully.'
        ];
        return response()->json($response, 200);
    }
}