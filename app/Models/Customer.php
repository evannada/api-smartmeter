<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'customer_name', 'customer_phone', 'customer_email',
        'customer_address', 'customer_website', 'image_customer',         
        'pic_name', 'pic_phone', 'pic_email', 'status'
    ];

    public function company()
    {
      return $this->belongsTo(Company::class);
    }

    public function users()
    {
      return $this->hasMany(User::class);
    }

    public function gateways()
    {
      return $this->hasMany(Gateway::class);
    }

    public function nodes()
    {
      return $this->hasMany(Node::class);
    }
}