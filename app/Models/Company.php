<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'networkserver_id', 'company_name', 'company_phone',
      'company_email', 'company_address', 'company_website',
      'image_company', 'pic_name', 'pic_phone', 'pic_email',
      'method', 'url_middleware', 'mqtt_username', 'mqtt_password',
      'mqtt_port', 'status',
    ];

    public function networkserver()
    {
      return $this->belongsTo(Networkserver::class);
    }

    public function companies()
    {
      return $this->hasMany(Company::class);
    }

    public function users()
    {
      return $this->hasMany(User::class);
    }

    public function gateways()
    {
      return $this->hasMany(Gateway::class);
    }

    public function nodes()
    {
      return $this->hasMany(Node::class);
    }

    public function vouchers()
    {
      return $this->hasMany(Voucher::class);
    }

    public function pricelists()
    {
      return $this->hasMany(Pricelist::class);
    }
}