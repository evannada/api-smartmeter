<?php

class Helpers
{
    
    public static function storeImageBase64encode($base64encode, $path_folder)
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $base64encode)) {
            $image_base64encode = substr($base64encode, strpos($base64encode, ",")+1);
            $base64encode_name = time() . '.' . str_random(20) . '.jpg'; 
            $image_path = public_path() . $path_folder . $base64encode_name;
            file_put_contents($image_path, base64_decode($image_base64encode));
            $image_url = env('API_URL'). $path_folder . $base64encode_name;
        } else {
            $image_url = env('API_URL'). "/other/no_image.png";
        }

        return $image_url;
    }
}